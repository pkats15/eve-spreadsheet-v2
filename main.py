import csv,datetime,json
import requests #requests required (pip install requests)
from dateutil import parser as duparser
import os
import base64
import zipfile
import io
import xml.etree.ElementTree as et
import re

CSV_NAME = 'history.csv'
SPREADSHEET_NAME = "spreadsheet.xlsx"
ESI_URL = 'https://esi.evetech.net/latest'
HISTORY_URL = '/markets/{region_id}/history/'
DAY_MILLIS = 86400000


# function minutesFromIssue(date){
#   var now = Date.now()
#   var issued = new Date(date)
#   return (now-issued)/1000/60
# }

# function getDayMinus(date, days){
#   return new Date(date.getTime() - days*DAY_MILLIS)
# }

# function getToday(){
#   return getDayMinus(getTodayTemp(), 1)
# }

# function getTodayTemp(){
#   var td = new Date()
#   return new Date(td.getYear(), td.getMonth(), td.getDate())
# }

# function getJustDate(date){
#   return new Date(date.getYear(), date.getMonth(), date.getDate())
# }

# function getDateDiff(first, second){
#   var diff = (second - first)/DAY_MILLIS
#   return(Math.round(diff))
# }

# HISTORY

# function getItemHistory(region, item, period){
#   var url = "https://esi.tech.ccp.is/latest/markets/"
  
#   var history = []
  
#   var today = getToday()
#   var resp = UrlFetchApp.fetch(url + region + "/history/?type_id=" + item)
#   var json = JSON.parse(resp.getContentText())
#   //Process the orders
#   // for(var i=json.length-1; i>=json.length-period; i--){
#   var k = 0 //k indicates the absolute position (even including days missed) TOdO: Add better explanation
#   for(var i=0; i < period;){
#     var his = json[json.length - i - 1]
# //    if(his == undefined){
# //      history.push({date: "ERROR FOR ID: ", volume: 0, avg: 0, max: 0, min: 0})
# //      continue
# //    }
#     var date = getJustDate(new Date(his["date"]))
#     //If there is a gap mark zero volume, same avg price
#     var days_missed = (today - date) / DAY_MILLIS - k
#     if(days_missed > 0){
#       for(var j=0; j<days_missed; j++){
#         var jdate = getDayMinus(date, -(days_missed-j))
#         if((today-jdate)/DAY_MILLIS < period){
#           var avg = his["average"]
#           history.push({date: jdate, volume: 0, avg: avg, max: avg, min: avg})
#         }else{
#           break
#         }

#       }
#       k = (today - date)/DAY_MILLIS //update k
#     }
    
#     //If period is overshot break
#     if((today - date)/DAY_MILLIS >= (period)){
#       break;
#     }
    
#     history.push({date: date, volume: his["volume"], avg: his["average"], max: his["highest"], min: his["lowest"]})
#     k++
#     i++
#   }
#   return history
# }


# //Domain id: 10000043
# //Slasher id: 585

def replaceTextBetween(originalText, delimeterA, delimterB, replacementText):
    leadingText = originalText.split(delimeterA)[0]
    trailingText = originalText.split(delimterB)[1]

    return leadingText + delimeterA + replacementText + delimterB + trailingText

def chnage_source2(z_in, z_out, path):
    before = "File.Contents(\""
    after = "\"),"
    text = ""
    
    i = z_in.open("Formulas/Section1.m")
    o = z_out.open("Formulas/Section1.m", mode="w")

    text = i.read().decode("utf-8")
    final = replaceTextBetween(text, before, after, path)
    o.write(final.encode("utf-8"))

    i.close()
    o.close()

def copy_files(z_in, z_out, not_incl=[]):
    # files = ["Config/Package.xml", "[Content_Types].xml"]
    for f in z_in.namelist():
        if(f in not_incl):
            continue
        i = z_in.open(f)
        o = z_out.open(f, mode="w")
        o.write(i.read())
        i.close()
        o.close()


class ItemData:
    def __init__(self, data):
        width = 4
        i = 0

        self.version = int.from_bytes(data[0:width],"little") # must be 0
        assert self.version == 0
        i += width # 4

        self.pkg_len = int.from_bytes(data[i:i+width],"little")
        i += width # 8
        self.pkg = data[i:i+self.pkg_len]
        i += self.pkg_len

        self.perm_len = int.from_bytes(data[i:i+width],"little")
        i += width
        self.perm = data[i:i+self.perm_len]
        i += self.perm_len

        self.md_len = int.from_bytes(data[i:i+width],"little")
        i += width
        self.md = data[i:i+self.md_len]
        i += self.md_len

        self.permb_len = int.from_bytes(data[i:i+width],"little")
        i += width
        self.permb = data[i:i+self.permb_len]
        i += self.permb_len

    def export(self, pkg):
        width = 4
        
        data = bytearray()
        data += int(self.version).to_bytes(width, "little")
        data += int(len(pkg)).to_bytes(width, "little")
        data += pkg
        data += int(self.perm_len).to_bytes(width, "little")
        data += self.perm
        data += int(self.md_len).to_bytes(width, "little")
        data += self.md
        data += int(self.permb_len).to_bytes(width, "little")
        data += self.permb

        return data

def update_source_xml(xml_in, path):
    data = base64.b64decode(xml_in.getroot().text)

    itd = ItemData(data)

    pkg_stream_in = io.BytesIO(itd.pkg)
    pkg_stream_out = io.BytesIO()

    z_in = zipfile.ZipFile(pkg_stream_in, mode="r")
    z_out = zipfile.ZipFile(pkg_stream_out, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=9)

    copy_files(z_in, z_out, not_incl=["Formulas/Section1.m"]) # Copy files
    chnage_source2(z_in, z_out, path)

    z_in.close()
    z_out.close()
    
    pkg_stream_out.seek(0)
    new_pkg = pkg_stream_out.read()

    new_data = itd.export(new_pkg)
    encoded = base64.b64encode(new_data)

    xml_in.getroot().text = str(encoded, "utf-8")
    

def update_csv_location():
    script_directory = os.path.dirname(os.path.realpath(__file__))
    excel_path = os.path.join(script_directory, SPREADSHEET_NAME)
    excel_path2 = os.path.join(script_directory, "test.xlsx")
    csv_path = os.path.join(script_directory, CSV_NAME)

    excel_zip = zipfile.ZipFile(excel_path, mode="r")
    excel_zip_out = zipfile.ZipFile(excel_path2, mode="w", compression=zipfile.ZIP_DEFLATED, compresslevel=9)
    copy_files(excel_zip, excel_zip_out, not_incl=["customXml/item1.xml"])

    query_item = excel_zip.open("customXml/item1.xml", mode="r")
    query_item_out = excel_zip_out.open("customXml/item1.xml", mode="w")
    
    internal_xml = et.parse(query_item)
    update_source_xml(internal_xml, csv_path)
    internal_xml.write(query_item_out, encoding="utf-16", xml_declaration=True)
    
    query_item.close()
    excel_zip.close()
    query_item_out.close()
    excel_zip_out.close()

    os.replace(excel_path2, excel_path)

class EVEDate:
    python_date = 0

    def __init__(self, date='today'):
        if(type(date) == datetime.date):
            self.python_date=date
        elif(date=='today'):
            self.python_date=datetime.date.today()
        else:
            self.python_date=duparser.parse(date).date()

    def __sub__(self, other):
        if type(other) == datetime.date:
            return (self.python_date - other).days
        elif type(other) == int:
            return EVEDate(date=self - datetime.timedelta(days=other))
        elif type(other) == EVEDate:
            return self - other.python_date
        else:
            tp = type(other)
            raise Exception('EVEDate neither type, it\'s {0}'.format(tp))
    
    def __add__(self, other):
        return EVEDate(date=self.python_date + datetime.timedelta(days=other))

    def __str__(self):
        return str(self.python_date)
    
    def __repr__(self):
        return str('EVEDate(' + str(self.python_date)+ ')')

def convert_history_to_csv_friendly2(history, item_id, name):
    h2 = history.copy()
    h2[0]['id'] = item_id
    h2[0]['name'] = name
    for h in h2:
        h['date'] = str(h['date'].python_date)
        if 'id' not in h: h['id']=''
        if 'name' not in h: h['name']=''
    return h2

def convert_history_to_csv_friendly(history, item_id, name):
    h2 = history.copy()
    # h2[0]['first']=True
    for h in h2:
        h['date'] = str(h['date'].python_date)
        h['id'] = item_id
        h['name'] = name
        # if not 'first' in h: h['first']=False
    return h2


def export_history_to_csv(chistory):
    with open(CSV_NAME, 'w', newline='') as csvfile:
        fieldnames = ['id', 'name', 'date', 'volume', 'avg', 'min', 'max']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(chistory)


def get_item_history(item, region, period):

    print("Getting history for: "+str(item))

    history = []

    params = {'type_id': item}
    r = requests.request('GET', ESI_URL+HISTORY_URL.format(region_id=region), params=params)
    today = EVEDate()
    js = r.json()

    k = 0 # k indicates the absolute position (even including days missed)

    for i in range(0, period):
        his = js[len(js) - i - 1]
        date = EVEDate(his["date"])
        # If there is a gap mark zero volume, same avg price
        days_missed = (today - date) - k
        if(days_missed > 0):
            for j in range(0, days_missed):
                jdate = date+int(days_missed-j)
                if today-jdate < period:
                    avg = his['average']
                    history.append({'date': jdate, 'volume': 0, 'avg': avg, 'max': avg, 'min': avg})
                else:
                    break
            k = today - date # update k

        # If period is overshot break
        if today - date >= period:
            break
        
        history.append({'date': date, 'volume': his['volume'], 'avg': his['average'], 'max': his['highest'], 'min': his['lowest']})
        k+=1
        i+=1

    return history



if(__name__ == '__main__'):
    h1 = get_item_history(585,10000043,99)
    ch1 = convert_history_to_csv_friendly(h1, 585, "Slasher")
    h2 = get_item_history(586,10000043,99)
    ch2 = convert_history_to_csv_friendly(h2, 586, "Unknown")
    export_history_to_csv(ch1+ch2)
    update_csv_location()